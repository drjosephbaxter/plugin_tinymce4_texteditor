Title: plugin_tinymce4_texteditor
Author: Dr Joseph Baxter <joseph.baxter@nottingham.ac.uk>
Copyright: University of Nottingham 2017 onwards
Description:

plugin_tinymce4_texteditor allows Rogō to use the tinymce 4 text editor https://www.tinymce.com/

Installation:

1. Extract plugin_tinymce4_texteditor archive into the plugins/texteditor directory inside Rogō.
2. Install via the plugins/index.php admin screen.